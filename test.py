import darknet as dn
import threading
import gc
net = dn.load_net(str.encode("yolov3.cfg"),
                      str.encode("weights/yolov3_last.weights"), 0)
meta = dn.load_meta(str.encode("cfg/obj.data"))
def keyloop(name):
    dn.set_gpu(0)
    global net,meta
    r = dn.detect(net, meta, str.encode("cache/"+name+'.jpg'),0.25)
    keyloop=[]
    for i in range(len(r)):
        keyloop.append([str(r[i][0],'utf-8'),r[i][1],r[i][2][0],r[i][2][1]])
    def take2(elem):
        return elem[2]
    keyloop.sort(key=take2)
    if len(keyloop)>4:
        temp=0
        for i in range(len(keyloop)):
            try:
                if (keyloop[i+1-temp][2]/keyloop[i-temp][2])<1.07:
                    if (keyloop[i-temp][0]=='left' and keyloop[i-temp+1][0]=='right') or (keyloop[i-temp][0]=='right' and keyloop[i-temp+1][0]=='left'):
                        if keyloop[i-temp][1]<keyloop[i-temp+1][1]:
                            del keyloop[i-temp+1]
                            temp+=1
                    else:
                        if keyloop[i-temp][1]<keyloop[i-temp+1][1]<1.07:
                            if keyloop[i-temp][2]<keyloop[i-temp+1][2]<1.07 and keyloop[i-temp][2]<keyloop[i-temp+1][2]>0.93:
                                del keyloop[i-temp]
                                temp+=1
            except:
                pass
    print(keyloop)
    return keyloop

def maploop(name):
    dn.set_gpu(0)
    global net,meta
    r = dn.detect(net, meta, str.encode("cache/"+name+'.jpg'),0.1)
    maploop=[]
    for i in range(len(r)):
        maploop.append([str(r[i][0],'utf-8'),r[i][1],r[i][2][0],r[i][2][1]])
    def take2(elem):
        return elem[2]
    #maploop.sort(key=take2)
    p=0
    k=0
    b=0
    pl=[]
    kl=[]
    blo=[]
    mapl=[]
    for i in range(len(maploop)):
        if maploop[i][0]=='p':
            p+=1
            pl.append(maploop[i])
        elif maploop[i][0]=='k':
            k+=1
            kl.append(maploop[i])
        elif maploop[i][0]=='b':
            blo.append(maploop[i])
        if p>1:
            if pl[1][1]>pl[0][1]:
                del pl[0]
            else:
                del pl[1]
        if k>1:
            if kl[1][1]>kl[0][1]:
                del kl[0]
            else:
                del kl[1]
    blo.sort(key=take2)
    mapl.append(kl[0])
    mapl.append(pl[0])
    for i in range(len(blo)):
        mapl.append(blo[i])
    print(mapl)
    return mapl
