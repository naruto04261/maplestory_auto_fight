# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Python\Python36\Scripts\map.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!
import win32gui,win32api,win32con,time,win32com.client,win32process,win32ui
import pywinauto
import time
from pywinauto import application
import os 
import os.path
import cv2
import numpy as np
from natsort import natsorted
import test
import threading,gc
import random
import warnings
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow
import sys
warnings.simplefilter("ignore", UserWarning)
sys.coinit_flags = 2

"""
jump='h'
skill1='1'
skill2='3'
skill3='2'
skill4='g'
skill5='c'
skill6='v'
skilljump='b'
skillmove='d'
skillat='0'
"""
class Ui_Dialog(object):
    def _init_(self):
        #QMainWindow.__init__(self)
        object._init_(self)
        self.pack()
        self.setupUi
        self.retranslateUi
        self.pushButton2()
        self.pushButton()
        self.lineEdit = QLineEdit(self)
        self.stop=False
    def stop(self):
        self.stop=True
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(593, 464)
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(120, 10, 111, 361))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lineEdit = QtWidgets.QLineEdit(self.widget)
        self.lineEdit.setObjectName("lineEdit")
        self.verticalLayout.addWidget(self.lineEdit)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.verticalLayout.addWidget(self.lineEdit_3)
        self.lineEdit_5 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.verticalLayout.addWidget(self.lineEdit_5)
        self.lineEdit_7 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.verticalLayout.addWidget(self.lineEdit_7)
        self.lineEdit_9 = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_9.setObjectName("lineEdit_9")
        self.verticalLayout.addWidget(self.lineEdit_9)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 50, 81, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(260, 50, 81, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(10, 110, 81, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(240, 110, 111, 16))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(10, 180, 81, 16))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(260, 180, 81, 16))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(10, 240, 101, 20))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(Dialog)
        self.label_8.setGeometry(QtCore.QRect(260, 240, 101, 20))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(Dialog)
        self.label_9.setGeometry(QtCore.QRect(20, 310, 101, 20))
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(Dialog)
        self.label_10.setGeometry(QtCore.QRect(260, 310, 101, 20))
        self.label_10.setObjectName("label_10")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(60, 410, 93, 28))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_2.setGeometry(QtCore.QRect(170, 410, 93, 28))
        self.pushButton_2.setObjectName("pushButton_2")
        self.widget1 = QtWidgets.QWidget(Dialog)
        self.widget1.setGeometry(QtCore.QRect(340, 10, 111, 361))
        self.widget1.setObjectName("widget1")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.widget1)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.widget1)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.verticalLayout_2.addWidget(self.lineEdit_2)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.widget1)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.verticalLayout_2.addWidget(self.lineEdit_4)
        self.lineEdit_6 = QtWidgets.QLineEdit(self.widget1)
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.verticalLayout_2.addWidget(self.lineEdit_6)
        self.lineEdit_8 = QtWidgets.QLineEdit(self.widget1)
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.verticalLayout_2.addWidget(self.lineEdit_8)
        self.lineEdit_10 = QtWidgets.QLineEdit(self.widget1)
        self.lineEdit_10.setObjectName("lineEdit_10")
        self.verticalLayout_2.addWidget(self.lineEdit_10)
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        self.pushButton_2.clicked.connect( self.stop )
        self.pushButton.clicked.connect( self.main )

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "傷害技能1"))
        self.label_2.setText(_translate("Dialog", "傷害技能2"))
        self.label_3.setText(_translate("Dialog", "回血技能"))
        self.label_4.setText(_translate("Dialog", "左右範圍技能"))
        self.label_5.setText(_translate("Dialog", "輔助技能1"))
        self.label_6.setText(_translate("Dialog", "輔助技能2"))
        self.label_7.setText(_translate("Dialog", "跳躍(不能是alt)"))
        self.label_8.setText(_translate("Dialog", "連接繩索"))
        self.label_9.setText(_translate("Dialog", "瞬移技"))
        self.label_10.setText(_translate("Dialog", "傷害技3"))
        self.pushButton.setText(_translate("Dialog", "開始"))
        self.pushButton_2.setText(_translate("Dialog", "結束"))
        self.lineEdit.setText("1")
        self.lineEdit_2.setText("3")
        self.lineEdit_3.setText("2")
        self.lineEdit_4.setText("g")
        self.lineEdit_5.setText("c")
        self.lineEdit_6.setText("v")
        self.lineEdit_7.setText("h")
        self.lineEdit_8.setText("b")
        self.lineEdit_9.setText("d")
        self.lineEdit_10.setText("0")
    def main(self):
        global skill1
        global skill2
        global skill3
        global skill4
        global skill5
        global skill6
        global skilljump
        global jump
        global skillmove
        global skillat
        skill1=self.lineEdit.text()         #attack1
        skill2=self.lineEdit_2.text()       #attack2
        skill3=self.lineEdit_3.text()       #treat
        skill4=self.lineEdit_4.text()       #attackall
        skill5=self.lineEdit_5.text()       #sup1
        skill6=self.lineEdit_6.text()       #sup2
        jump=self.lineEdit_7.text()
        skilljump=self.lineEdit_8.text()
        skillmove=self.lineEdit_9.text()
        skillat=self.lineEdit_10.text()     #attack3
        hwnd = win32gui.FindWindow(None,"MapleStory")
        threadid,pid = win32process.GetWindowThreadProcessId(hwnd)
        global app1
        app1 = application.Application()
        app1.connect(handle = hwnd)
        dialogs = app1.windows(handle = hwnd)
        win32gui.ShowWindow(hwnd,1)
        win32gui.SetForegroundWindow(hwnd)
        
        while(self.stop):
            print(checkcurse())
            if checkcurse()==True:
                print(checkcurse())
                flag=0
                while(flag==0):
                    if checkhp()==False:
                        other(skill3)
                        other(skill4)
                    else:      
                        flag=reachkey()
                flag=0
                print('next')
                while(flag==0):
                    flag=solvekey()
                    time.sleep(2.5)
            else:
                
                for i in range(5):
                    other(skill1)
                    right()
                    right()
                for i in range(5):
                    other(skill2)
                    left()
                    left()


app = QtWidgets.QApplication(sys.argv)
Form = QMainWindow()
w = Ui_Dialog()
w.setupUi(Form)
Form.show()

#maple
def cutkey(root,name):
    # 讀取圖檔
    img = cv2.imread(root+str(name)+'.jpg')
    # 裁切區域的 x 與 y 座標（左上角）
    x = 480
    y = 162

    # 裁切區域的長度與寬度
    w = 420
    h = 110

    # 裁切圖片
    crop_img = img[y:y+h, x:x+w]
    cv2.imwrite(root+str(name+1)+'.jpg', crop_img)
def cutmap(root,name):
    # 讀取圖檔
    img = cv2.imread(root+str(name)+'.jpg')
    # 裁切區域的 x 與 y 座標（左上角）
    x = 0
    y = 0

    # 裁切區域的長度與寬度
    w = 250
    h = 150

    # 裁切圖片
    crop_img = img[y:y+h, x:x+w]
    cv2.imwrite(root+str(name+1)+'.jpg', crop_img)
def compareHist( stdimg, ocimg):
    stdimg = cv2.imread(str(stdimg), 0)
    ocimg = cv2.imread(str(ocimg), 0)
    stdimg = np.float32(stdimg)
    ocimg = np.float32(ocimg)
    stdimg = np.ndarray.flatten(stdimg)
    ocimg = np.ndarray.flatten(ocimg)
    imgocr = np.corrcoef(stdimg, ocimg)
    print(imgocr[0, 1])
    return imgocr[0, 1] > 0.96

def window_capture(filename):
    hwnd = win32gui.FindWindow(None,"MapleStory")  # 窗口的编号，0号表示当前活跃窗口
    # 根据窗口句柄获取窗口的设备上下文DC（Divice Context）
    hwndDC = win32gui.GetWindowDC(hwnd)
    # 根据窗口的DC获取mfcDC
    mfcDC = win32ui.CreateDCFromHandle(hwndDC)
    # mfcDC创建可兼容的DC
    saveDC = mfcDC.CreateCompatibleDC()
    # 创建bigmap准备保存图片
    saveBitMap = win32ui.CreateBitmap()
    # 获取监控器信息
    #MoniterDev = win32api.EnumDisplayMonitors(None, None)
    #w = MoniterDev[0][2][2]
    #h = MoniterDev[0][2][3]
    w=1366
    h=768
    # print w,h　　　#图片大小
    # 为bitmap开辟空间
    saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)
    # 高度saveDC，将截图保存到saveBitmap中
    saveDC.SelectObject(saveBitMap)
    # 截取从左上角（0，0）长宽为（w，h）的图片
    #saveDC.BitBlt((0, 0), (w, h), mfcDC, (0, 32), win32con.SRCCOPY)
    #492,212 842 282
    saveDC.BitBlt((0,0), (w, h), mfcDC, (0, 32), win32con.SRCCOPY)
    saveBitMap.SaveBitmapFile(saveDC, filename)

def capture(name):
    beg = time.time()
    window_capture(name)
    end = time.time()
    print(end - beg)

def keyboard(control):
    win32api.keybd_event(control,0,win32con.KEYEVENTF_EXTENDEDKEY,0)  
    win32api.Sleep(200)
    win32api.keybd_event(control,0,win32con.KEYEVENTF_EXTENDEDKEY | win32con.KEYEVENTF_KEYUP,0)
def right():
    keyboard(win32con.VK_RIGHT)
def left():
    keyboard(win32con.VK_LEFT)
def down():
    keyboard(win32con.VK_DOWN)
def up():
    keyboard(win32con.VK_UP)
def space():
    app1.MapleStory.type_keys("{VK_SPACE}")
def alt():
    app1.MapleStory.type_keys("{VK_MENU}")
def shift():
    app1.MapleStory.type_keys("{VK_SHIFT}")
def ctrl():
    app1.MapleStory.type_keys("{VK_CONTROL}")
def esc():
    app1.MapleStory.type_keys("{VK_ESCAPE}")
def other(key):
    key="%"+key
    app1.MapleStory.type_keys(key)
def downhump():
    win32api.keybd_event(win32con.VK_DOWN,0,win32con.KEYEVENTF_EXTENDEDKEY,0)  
    win32api.Sleep(200)
    other(jump)
    win32api.Sleep(100)
    win32api.keybd_event(win32con.VK_DOWN,0,win32con.KEYEVENTF_EXTENDEDKEY | win32con.KEYEVENTF_KEYUP,0)
def mkdir(path):

    folder = os.path.exists(path)

    if not folder:                   #判断是否存在文件夹如果不存在则创建为文件夹
        os.makedirs(path)            #makedirs 创建文件时如果路径不存在会创建这个路径
def save():
    
    rootdir = os.getcwd()            
    file = os.path.join(rootdir,'cache')
    mkdir(file)             #调用函数
    try:
        a=int(os.path.splitext(natsorted(os.listdir(file))[-1])[0])
    except:
        a=0
    capture(file+"\\"+str(a+1)+".jpg")
    return (a+1)
def solvekey():
    flag=0
    space()
    time.sleep(1)
    name=save()
    rootdir = os.getcwd()            
    file = os.path.join(rootdir,'cache')
    root = file+"\\"
    cutkey(root,name)
    li=test.keyloop(str(name+1))
    print(li)
    for i in range(len(li)):
        if li[i][0]=='left':left()
        elif li[i][0]=='right':right()
        elif li[i][0]=='up':up()
        elif li[i][0]=='down':down()
    if len(li)==0:
        flag=1
    return flag
def checkcurse():
    name=save()
    rootdir = os.getcwd()            
    file = os.path.join(rootdir,'cache')
    root = file+"\\"
    # 讀取圖檔
    img = cv2.imread(root+str(name)+'.jpg')
    # 裁切區域的 x 與 y 座標（左上角）
    x = 480
    y = 202

    # 裁切區域的長度與寬度
    w = 420
    h = 70

    # 裁切圖片
    crop_img = img[y:y+h, x:x+w]
    crop_img = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    stdimg=root+'0curse.jpg'
    stdimg = cv2.imread(str(stdimg), 0)
    print(stdimg.shape)
    stdimg = np.float32(stdimg)
    crop_img = np.float32(crop_img)
    stdimg = np.ndarray.flatten(stdimg)
    crop_img = np.ndarray.flatten(crop_img)
    imgocr = np.corrcoef(stdimg, crop_img)
    print(imgocr[0, 1])
    if imgocr[0, 1]>0.8:
        return True

    else:
        return False
def reachkey():
    flag=0
    name=save()
    rootdir = os.getcwd()            
    file = os.path.join(rootdir,'cache')
    root = file+"\\"
    cutmap(root,name)
    li=test.keyloop(str(name+1))
    if len(li)==2:
        if li[0][0]=='k':
            if li[0][2]/li[1][2]<=1.02 and li[0][2]/li[1][2]>=0.98:
                if li[0][3]/li[1][3]<=1.02 or li[0][3]/li[1][3]>=0.98:
                    flag=1
                else:
                    if li[0][3]<li[1][3]:  #k在p上面
                        other(skilljump)
                    else:
                        downjump()
            else:
                if li[0][2]>li[1][2]:
                    right()
                else:
                    left()
        else:
            if li[0][2]/li[1][2]<=1.02 and li[0][2]/li[1][2]>=0.98:
                if li[0][3]/li[1][3]>=1.02 or li[0][3]/li[1][3]<=0.98:
                    flag=1
                else:
                    if li[0][3]>li[1][3]:  #k在p上面
                        other(skilljump)
                    else:
                        downjump()
            else:
                if li[0][2]<li[1][2]:
                    right()
                else:
                    left()
    else:
        flag=1
    try:
        if abs(li[0][2]-li[1][2])>=2:
            if li[0][0]=='k':
                for i in range(round(abs(li[0][2]-li[1][2])/3)):
                    left()
                if li[0][3]<li[1][3]:
                    other(skilljump)
                else:
                    downjump()
            else:
                for i in range(round(abs(li[0][2]-li[1][2])/3)):
                    right()
                if li[0][3]>li[1][3]:
                    other(skilljump)
                else:
                    downjump()
            time.sleep(1)
    except:
        flag=1
    return flag
def checkhp():
    name=save()
    print(str(name))
    rootdir = os.getcwd()            
    file = os.path.join(rootdir,'cache')
    root = file+"\\"
    # 讀取圖檔
    img = cv2.imread(root+str(name)+'.jpg')
    # 裁切區域的 x 與 y 座標（左上角）
    x = 590
    y = 715

    # 裁切區域的長度與寬度
    w = 95
    h = 13

    # 裁切圖片
    crop_img = img[y:y+h, x:x+w]
    crop_img = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    stdimg=root+'0hpcheck.jpg'
    stdimg = cv2.imread(str(stdimg), 0)
    stdimg = np.float32(stdimg)
    crop_img = np.float32(crop_img)
    stdimg = np.ndarray.flatten(stdimg)
    crop_img = np.ndarray.flatten(crop_img)
    imgocr = np.corrcoef(stdimg, crop_img)
    #print('11111111',imgocr[0, 1])
    if imgocr[0,1]>0.8:
        return True
    else:
        return False
###########################################################################################################################
app.exec_()
